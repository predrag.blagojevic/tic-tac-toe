import * as React from 'react';
import configureStore from './state/configureStore';

import App from './App';

it('renders without crashing', () => {
  const { store } = configureStore();

  shallow((
    <App store={store} />
  );
});
