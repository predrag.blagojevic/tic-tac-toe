import * as React from 'react';
import { Store } from 'redux';
import { Provider } from 'react-redux';
import { History } from 'history';
import { ConnectedRouter } from 'connected-react-router';

import { ActionObject } from 'src/utilities/redux';
import { State } from 'src/state/state';
import { ActionMap, ActionTypes } from 'src/state/actions';

import Routes from './components/views';
import Header from './components/views/Header';
import Footer from './components/views/Footer';

interface Props {
  store: Store<State, ActionObject<ActionMap, ActionTypes>>;
  history: History;
}

const baseClass = 'ttt-app';

const App: React.SFC<Props> = ({ store, history }) => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div id="app" className={baseClass}>
          <Header />
          <section className="ttt-body">
            <Routes />
          </section>
          <Footer />
        </div>
      </ConnectedRouter>
    </Provider>
  );
};

export default App;
