import * as React from 'react';
import classNames from 'classnames';

type LinkTargets = '_blank' | '';

const baseClass = 'ttt-link';

interface Props {
  name?: string;
  href: string;
  rel?: string;
  target?: LinkTargets;
  className?: string;
  disabled?: boolean;
}

const Anchor: React.SFC<Props> = props => {
  const { children, href, rel, target, className } = props;
  const rels = classNames('noopener', 'noreferrer', rel);
  const classes = classNames(baseClass, className);

  return (
    <a {...props} href={href} rel={rels} target={target} className={classes}>
      {children}
    </a>
  );
};

export default Anchor;
