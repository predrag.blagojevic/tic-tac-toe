import * as React from 'react';
import { Field, Range } from 'src/types';

import Board from './Board';

describe('Board', () => {
  const fields = [
     0,  0, -1,
     0,  1,  0,
    -1,  0,  0
  ];

  it('renders without crashing', () => {
    shallow(<Board fields={fields} />);
  });
});
