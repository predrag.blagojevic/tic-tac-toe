import * as React from 'react';

import BoardField from '@components/Field';
import { Field, Range } from 'src/types';
import { getCombination } from 'src/utilities/game';

interface Props {
  fields: Field[];
  onMove: (field: Range) => void;
}

const baseClass = 'ttt-board';

class Board extends React.PureComponent<Props> {
  handleClick = (index: Range) => {
    const { onMove } = this.props;

    onMove(index);
  };

  render() {
    const { fields } = this.props;
    const line = getCombination(fields);

    return (
      <ol className={baseClass}>
        {fields.map((field: Field, idx: Range) => (
          <BoardField
            key={idx}
            index={idx}
            value={field}
            crossed={line.includes(idx)}
            onClick={this.handleClick}
          />
        ))}
      </ol>
    )
  };
}

export default Board;
