import * as React from 'react';

import BoardField from './Field';

describe('Field', () => {
  it('renders without crashing', () => {
    const field = shallow(<BoardField index="0" value="1" />);

    expect(field.hasClass('ttt-field--x')).toEqual(true);
  });
});
