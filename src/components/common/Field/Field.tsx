import * as React from 'react';
import classNames from 'classnames';

import { Range , Field } from 'src/types';

import SVGX from '@images/icon-x.svg';
import SVGO from '@images/icon-o.svg';

const baseClass = 'ttt-field';

interface Props {
  index: Range;
  value: Field;
  crossed?: boolean;
  onClick: (index: Range) => void;
}

class BoardField extends React.PureComponent<Props> {
  handleClick = () => {
    const { index, onClick = () => undefined } = this.props;

    onClick(index);
  };

  render() {
    const { value, crossed } = this.props;

    const sign: string = value !== Field.Empty ? Field[value].toLowerCase() : '';

    const classes = classNames(
      baseClass,
      sign && `${baseClass}--${sign}`,
      crossed && `${baseClass}--crossed`,
    );

    return (
      <li className={classes} onClick={this.handleClick}>
        {value === Field.X && (
          <SVGX className={`${baseClass}__sign`} />
        )}
        {value === Field.O && (
          <SVGO className={`${baseClass}__sign`} />
        )}
      </li>
    );
  }
}

export default BoardField;
