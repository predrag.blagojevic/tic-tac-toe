import * as React from 'react';
import classNames from 'classnames';

import LogoSVG from '@images/logo-5.svg';

const baseClass = 'ttt-logo';

type LogoSizes = 'small' | 'medium' | 'large';

interface Props {
  text?: string;
  size?: LogoSizes;
  className?: string;
}

const Logo: React.SFC<Props> = props => {
  const { text = '', size, className } = props;

  const classes = classNames(
    baseClass,
    size && `${baseClass}--${size}`,
    className
  );

  return (
    <figure className={classes}>
      <LogoSVG className={`${baseClass}__image`} />
      <figcaption className={`${baseClass}__text`}>{text}</figcaption>
    </figure>
  );
};

export default Logo;
