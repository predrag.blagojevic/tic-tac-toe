import * as React from 'react';
import classNames from 'classnames';

const baseClass = 'ttt-score';

interface Props {
  label: string;
  value: number;
  winner?: boolean;
  className?: string;
}

const Score: React.SFC<Props> = props => {
  const { label, value, winner, className } = props;

  const classes = classNames(
    baseClass,
    winner && `${baseClass}--winner`,
    className
  );

  return (
    <dl className={classes}>
      <dt className={`${baseClass}__label`}>{label}</dt>
      <dd className={`${baseClass}__value`}>{value}</dd>
    </dl>
  );
};

export default Score;
