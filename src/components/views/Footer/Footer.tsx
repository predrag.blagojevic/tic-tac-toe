import * as React from 'react';
import Anchor from '@components/Anchor';

const baseClass = 'ttt-footer';

const Footer: React.SFC = () => (
  <footer className={baseClass}>
    # Tic-tac-toe with React/Redux by{' '}
    <Anchor
      href="https://www.linkedin.com/in/ferlauf/"
      target="_blank"
      rel="external">
      Predrag Blagojevic
    </Anchor>
  </footer>
);

export default Footer;
