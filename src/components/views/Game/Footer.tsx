import * as React from 'react';

import { Mode, Player } from 'src/types';

import Icon from '@components/Icon';
import Label from '@components/Label';
import Switch from '@components/Switch';

import SVG1Player from '@images/icon-1player.svg';
import SVG2Players from '@images/icon-2players.svg';

interface Props {
  mode: Mode;
  player: Player;
  onModeChange: (mode: Mode) => void;
}

const baseClass = 'ttt-game-footer';

class GameFooter extends React.PureComponent<Props> {
  handleChange = (value: boolean) => {
    const { onModeChange } = this.props;

    onModeChange(value ? Mode.OnePlayer : Mode.TwoPlayers);
  };

  render() {
    const { mode, player } = this.props;

    return (
      <footer className={baseClass}>
        <Label text="Player:" className={`${baseClass}__player`}>
          <kbd className={`${baseClass}__player__current`}>
            {Player[player]}
          </kbd>
        </Label>
        <Label text="Mode:" type="switch" className={`${baseClass}__mode`}>
          <Switch
            name="swComputer"
            checked={mode === Mode.OnePlayer}
            onChange={this.handleChange}
            className={`${baseClass}__mode__change`}
          />
          <Icon
            data={SVG2Players}
            size="medium"
            className={`${baseClass}__mode__friend`}
            title="Human"
          />
          <Icon
            data={SVG1Player}
            size="medium"
            className={`${baseClass}__mode__computer`}
            title="Computer"
          />
        </Label>
      </footer>
    );
  }
};

export default GameFooter;
