import * as React from 'react';
import classNames from 'classnames';

import { Mode, Field, Range, Player, Score } from 'src/types';
import Board from '@components/Board';

import GameHeader from './Header';
import GameFooter from './Footer';

interface Props {
  mode: Mode;
  board: Field[];
  player: Player;
  scores: Score[];
  winner?: Player;
  gameOver: boolean;
  handleMove: (field: Range) => void;
  handleModeChange: (mode: Mode) => void;
}

const baseClass = 'ttt-game';

const classes = ({ gameOver }: Props) => classNames(
  baseClass,
  gameOver && `${baseClass}--over`,
);

const Game: React.SFC<Props> = (props: Props) => {
  const {
    mode,
    board,
    player,
    scores,
    winner,
    gameOver,
    handleMove ,
    handleModeChange,
  } = props;

  return (
    <div className={classes(props)}>
      <GameHeader mode={mode} scores={scores} winner={winner} />
      {gameOver &&
        <p className={`${baseClass}__message`}>Click to continue</p>
      }
      <Board fields={board} onMove={handleMove} />
      <GameFooter player={player} mode={mode} onModeChange={handleModeChange} />
    </div>
  );
};

export default Game;
