import * as React from 'react';

import { Mode, Player, Score as ScoreType } from 'src/types';

import Score from '@components/Score';

interface Props {
  mode: Mode;
  scores: ScoreType[];
  winner?: Player;
}

const baseClass = 'ttt-game-header';

class GameHeader extends React.PureComponent<Props> {
  render() {
    const { mode, scores, winner } = this.props;
    const { X, O, draw } = scores[mode];

    const labelX = mode === Mode.OnePlayer ? 'Player (X)' : 'Player 1 (X)';
    const labelO = mode === Mode.OnePlayer ? 'Computer (O)' : 'Player 2 (O)';

    return (
      <header className={baseClass}>
        <Score label={labelX} value={X} winner={winner === Player.X} />
        <Score label="Draw" value={draw} winner={winner === Player.Noone} />
        <Score label={labelO} value={O} winner={winner === Player.O} />
      </header>
    )
  }
}

export default GameHeader;
