import { connect } from 'react-redux';
import { compose, lifecycle, setStatic, withHandlers } from 'recompose';
import { Board, Range, Player, Mode } from 'src/types';
import { State } from 'src/state/state';

import {
  newGame,
  changeMode,
  changeBoard,
  changePlayer,
  changeScores,
  changeWinner,
  endGame,
} from 'src/state/actions';
import {
  getMode,
  getBoard,
  getPlayer,
  getScores,
  getWinner,
  isGameOver,
} from 'src/state/selectors';

import { play } from 'src/utilities/game';

import Game from './Game';

const mapStateToProps = (state: State) => ({
  mode: getMode(state),
  board: getBoard(state),
  player: getPlayer(state),
  scores: getScores(state),
  winner: getWinner(state),
  gameOver: isGameOver(state),
});

const actions = {
  newGame,
  changeMode,
  changeBoard,
  changePlayer,
  changeScores,
  changeWinner,
  endGame,
};

export default compose<any, any>(
  connect(mapStateToProps, actions),

  lifecycle<any, any>({
    componentDidMount() {
      this.props.newGame();
    },
  }),

  withHandlers({
    handleMove: ({
      mode,
      board,
      player,
      scores,
      winner,
      gameOver,
      newGame,
      changeBoard,
      changePlayer,
      changeWinner,
      endGame,
    }) => (field: Range): void => {
      play({
        mode,
        board,
        player,
        scores,
        field,
        winner,
        gameOver,
      })({
        onNewGame: newGame,
        onBoardChange: changeBoard,
        onScoresChange: changeScores,
        onPlayerChange: changePlayer,
        onWinnerChange: changeWinner,
        onGameOver: endGame,
      });
    },

    handleModeChange: ({ newGame, changeMode, changePlayer }) => (mode: Mode) => {
      changeMode(mode);
      changePlayer(Player.X);
      newGame();
    }
  }),
)(Game);
