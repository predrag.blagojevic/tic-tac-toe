import * as React from 'react';
import { Link } from 'react-router-dom';
import { History } from 'history';

import { GAME_PAGE, LEADERBOARDS_PAGE } from 'src/constants/routes';

import Logo from '@components/Logo';

interface Props {
  history: History;
}

const baseClass = 'ttt-header';

const Header: React.SFC<Props> = (props: Props) => {
  const { history: { location: { pathname }}} = props;

  return (
    <header className={baseClass}>
      <Logo text="Tic-tac-toe" />

      <h1 className={`${baseClass}__title`}>Tic-tac-toe</h1>

      <nav className={`${baseClass}__actions`}>
        <menu className={`${baseClass}__menu`}>
          <li className={`${baseClass}__item`}>
            <Link to={GAME_PAGE} className={`ttt-link`} aria-current={pathname === GAME_PAGE}>
              Play
            </Link>
          </li>
          <li className={`${baseClass}__item`}>
            <Link to={LEADERBOARDS_PAGE} className={`ttt-link`} aria-current={pathname === LEADERBOARDS_PAGE}>
              Leaderboards
            </Link>
          </li>
        </menu>
      </nav>
    </header>
  );
};

// @ts-ignore
export default Header;
