import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

import Header from './Header';

export default compose<any, any>(withRouter)(Header);
