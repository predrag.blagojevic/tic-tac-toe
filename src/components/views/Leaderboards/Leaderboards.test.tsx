import * as React from 'react';
import * as faker from 'faker';
import { User } from 'src/types';

import Leaderboards from './Leaderboards';

describe('Leaderboards', () => {
  const users: User[] = Array.from({ length: 1 }, () => ({
    id: faker.random.number({ min: 1, max: 999 }),
    name: faker.internet.userName(),
    avatar: faker.internet.avatar(),
    score: faker.random.number({ min: 100, max: 9999 }),
  }));

  it('renders without crashing', () => {
    shallow((
      <Leaderboards users={users} />
    );
  });
});
