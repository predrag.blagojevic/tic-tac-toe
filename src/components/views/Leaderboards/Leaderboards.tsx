import * as React from 'react';

import Loader from '@components/Loader';
import Avatar from '@components/Avatar';
import Button from '@components/Button';
import { User } from 'src/types';

const baseClass = 'ttt-leaderboards';

interface Props {
  users: User[];
  isLoading: boolean;
  error: string | null;
  handleFetchMore: (since: number) => void;
}

const Leaderboards: React.SFC<Props> = (props: Props) => {
  const { users, isLoading, error, handleFetchMore } = props;

  const getFirst = (users: User[]) =>
    users && users.length ?
      users.reduce((min, user) => Math.min(min, user.id), users[0].id)
      : 0;

  const getLast = (users: User[]) =>
    users && users.length ?
      users.reduce((max, user) => Math.max(max, user.id), users[0].id)
      : 0;

  const handleClick = () => handleFetchMore(getLast(users));

  return (
    <div className={baseClass}>
      <header className={`${baseClass}__header`}>
        <h5 className={`${baseClass}__title`}>User</h5>
        <h5 className={`${baseClass}__title`}>Score</h5>
      </header>
      <section className={`${baseClass}__content`}>
        {isLoading && <Loader />}
        {!isLoading && error && <p className="ttt-text--error">{ error }</p>}
        {!isLoading && !error && (
          <ol className={`${baseClass}__list`} start={getFirst(users)}>
            {users.map(({ id, avatar, name, score }) => (
              <li key={name} className={`${baseClass}__list__item`} value={id}>
                <div className={`${baseClass}__player`}>
                  <Avatar src={avatar} size="small" className={`${baseClass}__player__avatar`} />
                  <span className={`${baseClass}__player__name`}>{name}</span>
                  <b className={`${baseClass}__player__score`}>{score}</b>
                </div>
              </li>
            ))}
          </ol>
        )}
      </section>
      <footer className={`${baseClass}__footer`}>
        <Button
          type="tertiary"
          busy={isLoading}
          onClick={handleClick}
        >Load more...</Button>
      </footer>
    </div>
  )
};

export default Leaderboards;