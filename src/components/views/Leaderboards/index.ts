import { connect } from 'react-redux';
import { compose, lifecycle, withHandlers } from 'recompose';

import { State } from 'src/state/state';
import { fetchUsers } from 'src/state/actions';
import { getUsers, isFetchUsersInProgress, getUserError } from 'src/state/selectors';

import Leaderboards from './Leaderboards';

const mapStateToProps = (state: State) => ({
  users: getUsers(state),
  isLoading: isFetchUsersInProgress(state),
  error: getUserError(state),
});

const actions = {
  fetchUsers
};

export default compose(
  connect(mapStateToProps, actions),

  lifecycle<any, any>({
    componentDidMount() {
      this.props.fetchUsers(0);
    }
  }),

  withHandlers({
    handleFetchMore: ({ fetchUsers }) => (since: number): void => {
      fetchUsers(since);
    },
  }),
)(Leaderboards);
