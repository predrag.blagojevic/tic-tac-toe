import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { GAME_PAGE, LEADERBOARDS_PAGE } from 'src/constants/routes';

import Game from './Game';
import Leaderboards from './Leaderboards';

export default () => (
  <Switch>
    <Route exact path="/" component={() => <Redirect to={GAME_PAGE} />} />
    <Route path={GAME_PAGE} component={Game} />
    <Route path={LEADERBOARDS_PAGE} component={Leaderboards} />
  </Switch>
);
