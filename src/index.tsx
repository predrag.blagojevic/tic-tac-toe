import * as React from 'react';
import * as ReactDOM from 'react-dom';

import '@styles/index.scss';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './state/configureStore';

const { history, store } = configureStore();

ReactDOM.render(
  <App history={history} store={store} />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
