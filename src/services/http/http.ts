type HttpInterceptor<T> = Array<Promise<T>>;

interface HttpInterceptors {
  request: HttpInterceptor<Request>;
  response: HttpInterceptor<Response>;
}

const enum RequestTypes {
  GET = 'GET',
  POST = 'POST',
}

const DEFAULT_REQUEST_CONFIG: RequestInit = {
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
  },
};

class Http {
  private interceptors: HttpInterceptors = {
    request: [],
    response: [],
  };

  constructor(
    private baseURL: string = '',
    private retryCount: number = 1,
    private requestTimeoutSeconds: number = 60
  ) {
    if (this.retryCount > 0) {
      // retry requests that return server errors
      this.attachResponseInterceptor({
        response: (response: Response) => response,
        responseError: (response: Response, args: any[]) => {
          const retryCount = args[2] || 0;

          if (response.status >= 500 && retryCount < this.retryCount) {
            return this._fetch.apply(this, [
              ...args.slice(0, 2),
              retryCount + 1,
            ]);
          }
          return response;
        },
      });
    }
  }

  public get(url: string, params?: object) {
    return this.performRequest(RequestTypes.GET, url, params);
  }

  public post(url: string, data: object) {
    return this.performRequest(RequestTypes.POST, url, data);
  }

  public async fetch(
    requestUrl: string,
    fetchOptions: RequestInit
  ): Promise<Response> {
    const response = await fetch(requestUrl.toString(), fetchOptions);

    if (this.isStatusOK(response)) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  }

  public attachRequestInterceptor(interceptor: any) {
    this.interceptors.request.unshift(interceptor);
  }

  public attachResponseInterceptor(interceptor: any) {
    this.interceptors.response.unshift(interceptor);
  }

  private _fetch(...args: any[]) {
    if (args[1].signal.aborted) {
      return null;
    }

    return this.interceptors.request.length || this.interceptors.response.length
      ? this.intercept.apply(this, args)
      : this.fetch.apply(this, args);
  }

  private intercept<Promise>(...args: any[]) {
    let interceptedRequest = Promise.resolve(args);

    this.interceptors.request.forEach(({ request }: any) => {
      interceptedRequest = interceptedRequest.then(args => request(...args));
    });

    let interceptedResponse = interceptedRequest.then(args =>
      this.fetch.apply(this, args)
    );

    this.interceptors.response.forEach(({ response, responseError }: any) => {
      interceptedResponse = interceptedResponse.then(
        res => response(res, args),
        err => responseError(err, args)
      );
    });

    return interceptedResponse;
  }

  private performRequest(type: string, url: string, data: object = {}) {
    const requestUrl: URL = new URL(
      url.indexOf('http') === 0 ? url : `${this.baseURL}${url}`
    );

    const controller = new AbortController();
    const signal = controller.signal;

    const fetchOptions: RequestInit = {
      ...DEFAULT_REQUEST_CONFIG,
      method: type,
      signal,
    };

    if (type === RequestTypes.POST) {
      fetchOptions.body = JSON.stringify(data);
    } else {
      Object.entries(data).forEach(([key, value]) => {
        requestUrl.searchParams.append(key, value);
      });
    }

    return new Promise(async (resolve, reject) => {
      const timeout = setTimeout(() => {
        reject({ message: 'Request timed-out' });

        controller.abort();
      }, this.requestTimeoutSeconds * 1000);

      try {
        const response = await this._fetch(requestUrl.toString(), fetchOptions);

        clearTimeout(timeout);

        // Don't process user-aborted requests
        if (response.name === 'AbortError') {
          return;
        }

        if (this.isStatusOK(response)) {
          resolve(response.json());
        } else {
          const error = await this.formatResponseError(response);
          reject(error);
        }
      } catch (error) {
        reject(error);
      }
    });
  }

  private async formatResponseError(response: Response) {
    const { error } = await response.json();

    return {
      code: response.status,
      message: error,
    };
  }

  private isStatusOK(response: Response): boolean {
    return response.status >= 200 && response.status < 300;
  }
}

export default Http;
