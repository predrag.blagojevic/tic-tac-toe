import {
  ActionMap as GameActionMap,
  ActionTypes as GameActionTypes,
} from './game/actions';

import {
  ActionMap as UsersActionMap,
  ActionTypes as UsersActionTypes,
} from './users/actions';

export type ActionMap =
  GameActionMap &
  UsersActionMap;

export type ActionTypes =
  GameActionTypes |
  UsersActionTypes;

export * from './game/actionCreators';
export * from './users/actionCreators';
