import { createAction } from 'src/utilities/redux';
import { Mode, Field, Player, Score } from 'src/types';

import { ActionMap, ActionTypes } from './actions';

export const newGame = createAction<
  ActionMap,
  ActionTypes.NewGame
>(ActionTypes.NewGame);

export const changeMode = (mode: Mode) => createAction<
  ActionMap,
  ActionTypes.ChangeModeGame
>(ActionTypes.ChangeModeGame)(mode);

export const changeBoard = (board: Field[]) => createAction<
  ActionMap,
  ActionTypes.ChangeBoardGame
>(ActionTypes.ChangeBoardGame)(board);

export const changePlayer = (player: Player) => createAction<
  ActionMap,
  ActionTypes.ChangePlayerGame
>(ActionTypes.ChangePlayerGame)(player);

export const changeScores = (score: Score[]) => createAction<
  ActionMap,
  ActionTypes.ChangeScoresGame
>(ActionTypes.ChangeScoresGame)(score);

export const changeWinner = (player: Player) => createAction<
  ActionMap,
  ActionTypes.ChangeWinnerGame
>(ActionTypes.ChangeWinnerGame)(player);

export const endGame = createAction<
  ActionMap,
  ActionTypes.EndGame
>(ActionTypes.EndGame);
