import { Mode, Field, Player, Score } from 'src/types';

export enum ActionTypes {
  NewGame = '[Game] New Game',
  ChangeModeGame = '[Game] Change Game Mode',
  ChangeBoardGame = '[Game] Change Board Setup',
  ChangePlayerGame = '[Game] Change Current Player',
  ChangeScoresGame = '[Game] Change Scores',
  ChangeWinnerGame = '[Game] Change Winner',
  EndGame = '[Game] Game Over',
}

export interface ActionMap {
  [ActionTypes.NewGame]: void;
  [ActionTypes.ChangeModeGame]: Mode;
  [ActionTypes.ChangeBoardGame]: Field[];
  [ActionTypes.ChangePlayerGame]: Player;
  [ActionTypes.ChangeScoresGame]: Score[];
  [ActionTypes.ChangeWinnerGame]: Player;
  [ActionTypes.EndGame]: void;
}
