import { createReducer } from 'src/utilities/redux';

import { State, initialState } from './state';
import { ActionMap, ActionTypes } from './actions';

import { Mode, Board, Player, Score } from 'src/types';

import { emptyBoard } from 'src/utilities/game';

export default createReducer<State, ActionMap>(
  {
    [ActionTypes.NewGame]: (state): State => ({
      ...state,
      board: emptyBoard(),
      gameOver: false,
    }),

    [ActionTypes.ChangeModeGame]: (state, payload: Mode): State => ({
      ...state,
      mode: payload,
    }),

    [ActionTypes.ChangeBoardGame]: (state, payload: Board): State => ({
      ...state,
      board: payload,
    }),

    [ActionTypes.ChangePlayerGame]: (state, payload: Player): State => ({
      ...state,
      player: payload,
    }),

    [ActionTypes.ChangeScoresGame]: (state, payload: Score[]): State => ({
      ...state,
      scores: payload,
    }),

    [ActionTypes.ChangeWinnerGame]: (state, payload: Player): State => ({
      ...state,
      winner: payload,
    }),

    [ActionTypes.EndGame]: (state): State => ({
      ...state,
      gameOver: true,
    }),

  },
  initialState
);
