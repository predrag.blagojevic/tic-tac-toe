import { State } from './state';

export const getMode = (game: State) => game.mode;

export const getBoard = (game: State) => game.board;

export const getPlayer = (game: State) => game.player;

export const getScores = (game: State) => game.scores;

export const getWinner = (game: State) => game.winner;

export const isGameOver = (game: State) => game.gameOver;
