import { Mode, Field, Player, Score } from 'src/types';
import { emptyBoard } from 'src/utilities/game';

export interface State {
  mode: Mode;
  board: Field[];
  player: Player;
  scores: Score[];
  winner: Player | undefined;
  gameOver: boolean;
}

export const initialState: State = {
  mode: Mode.TwoPlayers,
  board: emptyBoard(),
  player: Player.X,
  scores: [
    { X: 0, O: 0, draw: 0 },
    { X: 0, O: 0, draw: 0 },
  ],
  winner: undefined,
  gameOver: false,
};
