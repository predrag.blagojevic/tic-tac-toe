import { combineReducers } from 'redux';

import { State as CombinedState } from './state';

import game from './game/reducer';
import users from './users/reducer';

export default combineReducers<CombinedState>({
  game,
  users
});
