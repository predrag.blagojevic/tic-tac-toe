import { State } from './state';

import * as fromGame from './game/selectors';
import * as fromUsers from './users/selectors';

export const getMode = ({ game }: State) => fromGame.getMode(game);
export const getBoard = ({ game }: State) => fromGame.getBoard(game);
export const getPlayer = ({ game }: State) => fromGame.getPlayer(game);
export const getScores = ({ game }: State) => fromGame.getScores(game);
export const getWinner = ({ game }: State) => fromGame.getWinner(game);
export const isGameOver = ({ game }: State) => fromGame.isGameOver(game);

export const getUsers = ({ users }: State) => fromUsers.getUsers(users);
export const isFetchUsersInProgress = ({ users }: State) => fromUsers.isFetchUsersInProgress(users);
export const getUserError = ({ users }: State) => fromUsers.getUserError(users);
