import { State as GameState } from './game/state';
import { State as UsersState } from './users/state';

export interface State {
  game: GameState;
  users: UsersState;
}
