import * as faker from 'faker';
import { ActionTypes } from './actions';
import { fetchUsers, fetchUsersSuccess, fetchUsersError } from './actionCreators';

describe('users actions', () => {
  it('should create [Users] Fetch action', () => {
    expect(fetchUsers(1)).toEqual({ type: ActionTypes.FetchUsers, payload: 1 });
  });

  it('should create [Users] Fetch Success action', () => {
    const users = Array.from({ length: 1 }, () => ({
      id: faker.random.number({ min: 1, max: 999 }),
      name: faker.internet.userName(),
      avatar: faker.internet.avatar(),
      score: faker.random.number({ min: 100, max: 9999 }),
    }));

    expect(fetchUsersSuccess(users)).toEqual({
      type: ActionTypes.FetchUsersSuccess,
      payload: users
    });
  });

  it('should create [Users] Fetch Error action', () => {
    const message = 'Error fetching users.';

    expect(fetchUsersError(message)).toEqual({
      type: ActionTypes.FetchUsersError,
      payload: message
    });
  });
});