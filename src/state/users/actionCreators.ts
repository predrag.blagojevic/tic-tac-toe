import { createAction } from 'src/utilities/redux';

import { ActionMap, ActionTypes } from './actions';

export const fetchUsers = (since: number) => createAction<
  ActionMap,
  ActionTypes.FetchUsers
>(ActionTypes.FetchUsers)(since);

export const fetchUsersSuccess = createAction<
  ActionMap,
  ActionTypes.FetchUsersSuccess
>(ActionTypes.FetchUsersSuccess);

export const fetchUsersError = (message: string) => createAction<
  ActionMap,
  ActionTypes.FetchUsersError
>(ActionTypes.FetchUsersError)(message);
