import { User } from 'src/types';

export enum ActionTypes {
  FetchUsers = '[Users] Fetch',
  FetchUsersSuccess = '[Users] Fetch Success',
  FetchUsersError = '[Users] Fetch Error',
}

export interface ActionMap {
  [ActionTypes.FetchUsers]: {};
  [ActionTypes.FetchUsersSuccess]: { users: User[] };
  [ActionTypes.FetchUsersError]: string;
}
