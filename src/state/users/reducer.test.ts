import * as faker from 'faker';
import { initialState } from './state';
import { fetchUsers, fetchUsersSuccess, fetchUsersError } from './actionCreators';
import reducer from './reducer';

describe('users reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(initialState, {})).toEqual(initialState);
  });

  it('should handle [Users] Fetch', () => {
    expect(reducer(initialState, fetchUsers(1))).toEqual({
      fetchUsersInProgress: true,
      users: [],
      fetchUsersError: null
    });
  });

  it('should handle [Users] Fetch Success', () => {
    const users = Array.from({ length: 1 }, () => ({
      id: faker.random.number({ min: 1, max: 999 }),
      name: faker.internet.userName(),
      avatar: faker.internet.avatar(),
      score: faker.random.number({ min: 100, max: 9999 }),
    }));

    expect(reducer(initialState, fetchUsersSuccess({ users }))).toEqual({
      fetchUsersInProgress: false,
      users: users,
      fetchUsersError: null
    });
  });

  it('should handle [Users] Fetch Error', () => {
    const message = 'Error fetching users.';

    expect(reducer(initialState, fetchUsersError(message))).toEqual({
      fetchUsersInProgress: false,
      users: [],
      fetchUsersError: message
    });
  });
});