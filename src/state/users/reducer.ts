import { createReducer } from 'src/utilities/redux';

import { State, initialState } from './state';
import { ActionMap, ActionTypes } from './actions';

export default createReducer<State, ActionMap>(
  {
    [ActionTypes.FetchUsers]: (state): State => ({
      ...state,
      fetchUsersInProgress: true,
      fetchUsersError: null,
    }),
    [ActionTypes.FetchUsersSuccess]: (state, { users }): State => ({
      ...state,
      users,
      fetchUsersInProgress: false,
      fetchUsersError: null,
    }),
    [ActionTypes.FetchUsersError]: (state, message): State => ({
      ...state,
      fetchUsersInProgress: false,
      fetchUsersError: message,
    }),
  },
  initialState
);
