import { all, put, takeLatest } from 'redux-saga/effects';
import * as faker from 'faker';
import http from 'src/services/http';
import { User } from 'src/types/user';

import * as actions from './actionCreators';
import { ActionTypes } from './actions';

const fakeResponse = () => Array.from({ length: 100 }, () => ({
  id: faker.random.number({ min: 1, max: 99 }),
  login: faker.internet.userName(),
  avatar_url: faker.internet.avatar(),
}));

function* fetchUsers$({ payload }: any) {
  try {
    const since: number = Number(payload) || 0;

    const response: any = yield http.get(`https://api.github.com/users?since=${since}`);
    // const response: any = fakeResponse();

    const users: User[] = response.map(({ id, login, avatar_url }: any) => ({
      id,
      name: login,
      avatar: avatar_url,
      score: Math.floor(Math.random() * (10000 - 100 + 1)) + 100
    }));
    // .sort((user1: User, user2: User) => user1.score > user2.score ? -1 : (user1.score < user2.score ? 1 : 0));
    // throw 'Testing error handling';

    yield put(actions.fetchUsersSuccess({ users }));
  } catch (e) {
    yield put(actions.fetchUsersError('Error fetching users.'));
  }
}

export default function*() {
  yield all([takeLatest(ActionTypes.FetchUsers, fetchUsers$)]);
}
