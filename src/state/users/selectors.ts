import { State } from './state';

export const getUsers = (users: State) => users.users;

export const isFetchUsersInProgress = (users: State) => users.fetchUsersInProgress;

export const getUserError = (users: State) => users.fetchUsersError;
