import { User } from 'src/types/user';

export interface State {
  fetchUsersInProgress: boolean;
  users: User[];
  fetchUsersError: string | null;
}

export const initialState: State = {
  fetchUsersInProgress: false,
  users: [],
  fetchUsersError: null,
};
