export enum Combination {
  None = -1,

  Upper = 0,
  Middle = 1,
  Lower = 2,

  Left = 3,
  Center = 4,
  Right = 5,

  Major = 6,
  Minor = 7,
}

