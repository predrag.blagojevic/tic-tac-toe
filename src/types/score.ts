export interface Score {
  X: number;
  O: number;
  draw: number;
}
