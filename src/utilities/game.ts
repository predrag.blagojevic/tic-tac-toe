import {
  Mode,
  Range,
  Field,
  Board,
  Player,
  Score,
  Combination,
} from 'src/types';
import { changePlayer } from '../state/game/actionCreators';


interface Occurrence {
  X: number;
  O: number;
}


interface Params {
  mode?: Mode;
  board: Board;
  player: Player;
  field: Range;
  scores: Score[];
  winner?: Player | undefined;
  gameOver?: boolean;
}


interface Actions {
  onNewGame?: () => void,
  onBoardChange: (board: Board) => void,
  onScoresChange: (scores: Score[]) => void,
  onPlayerChange: (player: Player) => void,
  onWinnerChange: (winner?: Player) => void,
  onGameOver?: () => void,
}


/**
 * Winning combinations
 *
 */
const winningCombinations = [
  // horizontal
  [0, 1, 2], // upper   0
  [3, 4, 5], // middle  1
  [6, 7, 8], // lower   2

  // vertical
  [0, 3, 6], // left    3
  [1, 4, 7], // center  4
  [2, 5, 8], // right   5

  // crossed
  [0, 4, 8], // major   6
  [2, 4, 6], // minor   7
];


/**
 * Returns empty board
 *
 */
const emptyBoard = (): Field[] => [
  0, 0, 0,
  0, 0, 0,
  0, 0, 0,
];


/**
 *
 * @param board
 */
const isEmpty = (board: Board): boolean =>
  board.every(value => value === Field.Empty);

/**
 *
 * @param board
 */
const isFull = (board: Board): boolean =>
  board.every(field => field !== Field.Empty);

/**
 *
 * @param board
 */
const getStatus = (board: Board): number[] =>
  winningCombinations.map(line => line.reduce((sum, index) => sum + board[index], 0));

/**
 * Check if the board state is valid
 *
 * @param board
 */
const isValid = (board: Board): boolean => {
  const permitted = board.every((field: Field) =>
    field === Field.X ||
    field === Field.O ||
    field === Field.Empty
  );

  if (!permitted) {
    return false;
  }

  const frequency: Occurrence = board.reduce(
    (result: Occurrence, field: Field) => ({
      X: result.X + Number(field === Field.X),
      O: result.O + Number(field === Field.O),
    }),
    { X: 0, O: 0 }
  );

  if (Math.abs(frequency.X - frequency.O) > 1) {
    return false;
  }

  const status = getStatus(board);

  const sum: Occurrence = status.reduce(
    (result: Occurrence, value: number) => ({
      X: result.X + Number(value === 3),
      O: result.O + Number(value === -3),
    }),
    { X: 0, O: 0 }
  );

  return !(
    (sum.X > 0 && sum.O > 0) ||
    (sum.X > 1 || sum.O > 1)
  );
};

/**
 *
 * @param board
 * @param player
 */
const hasWon = (player: Player, board: Board): boolean =>
  getStatus(board).some(line => line === 3 * player);

/**
 *
 * @param board
 */
const hasWinner = (board: Board): boolean =>
  hasWon(Player.X, board) || hasWon(Player.O, board);

/**
 *
 * @param board
 */
const isDraw = (board: Board): boolean => isFull(board) && !hasWinner(board);

/**
 *
 * @param board
 */
const isOver = (board: Board): boolean =>
  isFull(board) || hasWinner(board) || isDraw(board);

/**
 *
 * @param board
 */
const isRunning = (board: Board): boolean => !isOver(board);

/**
 *
 * @param board
 */
const getWinner = (board: Board): Player =>
  hasWon(Player.X, board)
    ? Player.X
    : hasWon(Player.O, board)
      ? Player.O
      : Player.Noone;

/**
 *
 * @param board
 */
const getLoser = (board: Board): Player =>
  hasWon(Player.X, board)
    ? Player.O
    : hasWon(Player.O, board)
      ? Player.X
      : Player.Noone;

/**
 *
 * @param board
 */
const hasTurn = (board: Board): Player => {
  const sum: Occurrence = board.reduce(
    (result: Occurrence, field: Field) => ({
      X: result.X + Number(field === Field.X),
      O: result.O + Number(field === Field.O),
    }),
    { X: 0, O: 0 }
  );

  return sum.O >= sum.X ? Player.X : (
    sum.O < sum.X ? Player.O :
      Player.Noone
  );
};

/**
 *
 * @param board
 * @param idx
 */
const isOccupied = (board: Board, idx: Range): boolean =>
  board[idx] !== Field.Empty;

/**
 *
 * @param board
 * @param idx
 */
const isFree = (board: Board, idx: Range): boolean =>
  !isOccupied(board, idx);

/**
 *
 * @param player
 */
const switchPlayer = (player: Player): Player =>
  player === Player.X ? Player.O : Player.X;


/**
 *
 * @param board
 * @param player
 * @param idx
 */
const canMove = (board: Board, player: Player, idx: Range) =>
  isFree(board, idx);


/**
 * If there is a winning combination on board, return an array of indexes
 *
 * @param board
 */
const getCombination = (board: Board): number[] => {
  const status = getStatus(board);
  const index = status.findIndex((sum: number) => Math.abs(sum) === 3);

  return index < 0 ? [] : winningCombinations[index];
};


/**
 * Plays a move
 *
 * @param board
 * @param player
 * @param field
 */
const playMove = (
  board: Board,
  player: Player,
  idx: number,
): Board => {
  const nextBoard = [
    ...board.slice(0, idx),
    player === Player.X ? Field.X : Field.O,
    ...board.slice(idx + 1)
  ];

  return nextBoard;
};


/**
 *
 * @param mode
 * @param scores
 * @param winner
 */
const updateScores = (mode: Mode, scores: Score[], winner: Player): Score[] => {
  const nextScores = [...scores];

  if (winner === Player.Noone) {
    nextScores[mode].draw += 1;
  }
  else {
    nextScores[mode][Player[winner]] += 1;
  }

  return nextScores;
};


/**
 * Check if we can play to win or to block subject
 *
 * @param board
 * @param player
 * @param subject
 */
const smartMove = (board: Board, player: Player, subject: Player): Board => {
  const status = getStatus(board);  // status: [0, 2, -1, -2, 1, 2, 0, 1]
  const candidate = status.indexOf(subject * 2);

  // if there are no lines with 2 symbols on them
  if (candidate === -1) {
    return [];
  }

  const row = winningCombinations[candidate]; // row: [3, 4, 5]

  // 1. find an empty field
  const index = row
    .map(index => board[index] === 0 ? index : -1)
    .find(index => index > -1);

  // 2. play move on that field
  const nextBoard = playMove(board, player, Number(index));

  // 3. and return new board
  return nextBoard;
};

/**
 *
 * @param props
 * @param actions
 */
const turn = (props: Params, actions: Actions): boolean => {
  const { board, scores } = props;
  const { onBoardChange, onScoresChange, onWinnerChange, onGameOver, onPlayerChange } = actions;

  // 1. play move
  const nextBoard: Board = board;
  onBoardChange(nextBoard);

  // 2. do we have a winner?
  if (hasWinner(nextBoard)) {
    const winner: Player = getWinner(nextBoard);
    const nextScores = updateScores(Mode.OnePlayer, scores, winner);
    onScoresChange(nextScores);
    onWinnerChange(winner);
    onGameOver && onGameOver();
    onPlayerChange(Player.X);

    return true;
  }

  // 3. or it's draw
  else if (isDraw(nextBoard)) {
    const nextScores = updateScores(Mode.OnePlayer, scores, Player.Noone);
    onScoresChange(nextScores);
    onWinnerChange(Player.Noone);
    onGameOver && onGameOver();
    onPlayerChange(Player.X);

    return true;
  }

  return false;
};


/**
 *
 * @param player
 * @param board
 */
const playComputer = (
  board: Board,
  computer: Player,
  human: Player,
  props: Params,
  actions: Actions
): boolean => {
  if (isFull(board)) {
    return false;
  }

  const status = getStatus(board);  // status: [0, 2, -1, -2, 1, 2, 0, 1]

  let nextBoard: Field[];

  // 1. check if there is potential winning row
  nextBoard = smartMove(board, computer, computer);

  if (nextBoard.length) {
    turn({
      ...props,
      board: nextBoard,
      player: computer,
    }, actions);

    return true;
  }

  // 2. check if there is human's row to block him
  nextBoard = smartMove(board, computer, human);

  if (nextBoard.length) {
    turn({
      ...props,
      board: nextBoard,
      player: computer,
    }, actions);

    return true;
  }

  // 3. play some random move
  const empties = board
    .map((field: Field, index: number) =>
      field === Field.Empty ? index : undefined
    )
    .filter((i: number) => i);

  const index = Math.floor(Math.random() * empties.length);

  nextBoard = playMove(board, computer, Number(empties[index]));

  if (nextBoard.length) {
    turn({
      ...props,
      board: nextBoard,
      player: computer,
    }, actions);

    return true;
  }

  return false;
};


/**
 * @TODO: Optimize this!
 *
 * @param props
 */
const play = (props: Params) => {
  const {
    mode = Mode.TwoPlayers,
    board,
    player,
    field,
    scores,
    winner,
    gameOver,
  } = props;

  return function (actions: Actions) {
    const {
      onNewGame,
      onBoardChange,
      onScoresChange,
      onPlayerChange,
      onWinnerChange,
      onGameOver,
    } = actions;

    // 1. check if game is over
    if (isOver(board)) {
      onPlayerChange(Player.X);
      onNewGame && onNewGame();
      return;
    }

    // 2. check if player can move on desired field
    if (!canMove(board, player, field)) {
      return;
    }

    // 3. play move
    const nextBoard: Board = playMove(board, player, field);
    onBoardChange(nextBoard);

    // do we have a winner?
    if (hasWinner(nextBoard)) {
      const winner: Player = getWinner(nextBoard);
      const nextScores = updateScores(mode, scores, winner);

      onScoresChange(nextScores);
      onWinnerChange(winner);
      onGameOver && onGameOver();
      onPlayerChange(Player.X);
      // console.log(`And the winner is ${winner}! :)`);
    }

    // or it's draw
    else if (isDraw(nextBoard)) {
      const nextScores = updateScores(mode, scores, Player.Noone);

      onScoresChange(nextScores);
      onWinnerChange(Player.Noone);
      onGameOver && onGameOver();
      onPlayerChange(Player.X);
      // console.log(`Draw game... :|`);
    }

    // 4. switch players
    const nextPlayer: Player = switchPlayer(player);
    onPlayerChange(nextPlayer);

    // 5. optional - computer moves
    if (mode === Mode.OnePlayer && playComputer(nextBoard, nextPlayer, player, props, actions)) {
      onPlayerChange(switchPlayer(nextPlayer));
    }
  }
};


export {
  emptyBoard,
  getCombination,
  play,
}